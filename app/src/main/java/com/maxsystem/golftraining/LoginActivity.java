package com.maxsystem.golftraining;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.maxsystem.golftraining.R;
import com.maxsystem.golftraining.student.ListVideoActivity;

import java.lang.ref.WeakReference;


/**
 * Created by minhdinh on 9/20/16.
 */
public class LoginActivity extends BaseActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private static final int MY_READ_EXTERNAL_STORAGE=112;

    private Button mBtnLogin;

    private EditText mEdtAccount;

    private EditText mEdtPassWord;

    private final MyTextChangeListener myTextChangeListener = new MyTextChangeListener(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        initView();
        addAcctionListener();
        requestPermisstion();
    }

    private void requestPermisstion(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_READ_EXTERNAL_STORAGE);

                // READ_EXTERNAL_STORAGE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
    /**
     * init view
     */
    private void initView(){
        mBtnLogin = (Button)findViewById(R.id.btn_login);
        mEdtAccount = (EditText)findViewById(R.id.edt_account);
        mEdtPassWord = (EditText)findViewById(R.id.edt_password);
    }

    /**
     * add action to view
     */
    private void addAcctionListener(){
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                onHandleClickLoginButton();
            }
        });
        mEdtAccount.addTextChangedListener(myTextChangeListener);
        mEdtPassWord.addTextChangedListener(myTextChangeListener);
    }

    private void onHandleClickLoginButton(){
        String account = mEdtAccount.getText().toString();
        String pass = mEdtPassWord.getText().toString();
        if(validateAccount(account,pass)){
            //TODO Login
            startActivity(new Intent(this, ListVideoActivity.class));
        }
    }

    /**
     *
     * @param account
     * @param pass
     */
    private boolean validateAccount(String account, String pass){
        //TODO validate account
        if(TextUtils.isEmpty(account) || TextUtils.isEmpty(pass)){
            return false;
        }
        return true;
    }

    /**
     *
     * handle text change
     */
    private void onLoginTextChange(){
        if(TextUtils.isEmpty(mEdtPassWord.getText()) || TextUtils.isEmpty(mEdtPassWord.getText())){
            mBtnLogin.setEnabled(false);
        }else{
            mBtnLogin.setEnabled(true);
        }
    }

    private static class MyTextChangeListener implements TextWatcher{
        private final WeakReference mLoginActivityPref;

        MyTextChangeListener(LoginActivity activity){
            mLoginActivityPref = new WeakReference<>(activity);
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            LoginActivity activity = (LoginActivity) mLoginActivityPref.get();
            if(activity != null) {
                activity.onLoginTextChange();
            }
        }
    }
}
