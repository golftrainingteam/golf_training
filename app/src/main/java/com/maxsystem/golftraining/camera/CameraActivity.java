
package com.maxsystem.golftraining.camera;

import android.app.Activity;
import android.os.Bundle;
import com.maxsystem.golftraining.R;

/**
 * Created by minhdinh on 9/20/16.
 */
public class CameraActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cameara_activity);
        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, Camera2VideoFragment.newInstance())
                    .commit();
        }
    }

}
