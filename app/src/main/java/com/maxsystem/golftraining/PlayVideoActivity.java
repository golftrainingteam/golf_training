package com.maxsystem.golftraining;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;

import golshadi.majid.core.DownloadManagerPro;
import golshadi.majid.report.listener.DownloadManagerListener;

public class PlayVideoActivity extends AppCompatActivity implements DownloadManagerListener {

    private static final String TAG = PlayVideoActivity.class.getSimpleName();

    private ProgressBar progressBar;
    private DownloadManagerPro mDownloadManager;
    private Button btn_download;
    private Button btn_play;
    private Button btn_delete;
    private TextView tv_status;
    private String file_name;
    private String folder_name = "GopPho/";
    private VideoView videoView;
    private int position = 0;
    private BroadcastReceiver broadcastReceiver;
    private MediaController mediaControls;
    private boolean isAvaibleDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playvideo_activity);
        btn_download = (Button) findViewById(R.id.btn_download);
        btn_play = (Button) findViewById(R.id.btn_play);
        tv_status = (TextView) findViewById(R.id.tv_status);
        btn_delete = (Button)findViewById(R.id.btn_delete);
        videoView = (VideoView) findViewById(R.id.video_view);
        mediaControls = new MediaController(this);
        videoView.setMediaController(mediaControls);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        progressBar.setMax(100);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                //if we have a position on savedInstanceState, the video playback should start from here
                videoView.seekTo(position);
                Log.w(TAG,"Preare video = "+  position);
                if (position == 0) {
                    videoView.start();
                    isAvaibleDelete = false;
                    Log.w(TAG,"Run video");
                } else {
                    //if we come from a resumed activity, video playback will be paused
                    Log.w(TAG,"Pause video");
                    videoView.pause();
                    isAvaibleDelete = true;
                }
            }

        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                isAvaibleDelete = true;
                Log.w(TAG,"Video run out");

            }
        });

        mDownloadManager = new DownloadManagerPro(this.getApplicationContext());
        mDownloadManager.init(folder_name, 12, this);
        file_name = "video";
        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_download.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Start downloading...", Toast.LENGTH_SHORT).show();
                tv_status.setText("Downloading...");
                int taskToekn = mDownloadManager.addTask(file_name, "http://cogi.vn/bantinaaa.mp4", false, false);
                try {
                    mDownloadManager.startDownload(taskToekn);
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "Some error : " + e.toString(), Toast.LENGTH_SHORT).show();
                    btn_download.setEnabled(true);
                    e.printStackTrace();
                }
            }
        });

        videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                btn_play.setEnabled(true);
                btn_download.setEnabled(false);
                btn_delete.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Download Completed.Click Play!", Toast.LENGTH_SHORT).show();
                tv_status.setText("Done!.Click Play");

            }
        };
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setProgress(0);
                Uri vidFile = Uri.parse("/storage/emulated/0/GopPho/" + file_name + ".mp4");
                videoView.setVideoURI(vidFile);
                Log.e("getRootDirectory", Environment.getRootDirectory().getAbsolutePath());
                Log.e("getDataDirectory",
                        Environment.getDataDirectory().getAbsolutePath());

            }
        });
        IntentFilter intentFilter = new IntentFilter("DOWNLOAD");
        registerReceiver(broadcastReceiver, intentFilter);
        if(isExistFile("/storage/emulated/0/GopPho/" + file_name + ".mp4")){
            btn_play.setEnabled(true);
            btn_download.setEnabled(false);
            btn_delete.setEnabled(true);
        }else{
            btn_download.setEnabled(true);
            btn_play.setEnabled(false);
            btn_delete.setEnabled(false);
        }
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!videoView.isPlaying()){
                    File file = new File("/storage/emulated/0/GopPho/" + file_name + ".mp4");
                   boolean isDeleted =  file.delete();
                    if(isDeleted) {
                        btn_download.setEnabled(true);
                        btn_delete.setEnabled(false);
                        btn_play.setEnabled(false);
                    }else{
                        Toast.makeText(PlayVideoActivity.this,"Cant not delete video",Toast.LENGTH_LONG).show();

                    }
                }else{
                    Toast.makeText(PlayVideoActivity.this,"Cant not delete video when playing",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private boolean isExistFile(String path){
        File file = new File(path);
        Log.e("file"," = " +  file.exists());
        return file.exists();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override


    public void OnDownloadStarted(long taskId) {


    }

    @Override
    public void OnDownloadPaused(long taskId) {

    }

    @Override
    public void onDownloadProcess(long taskId, double percent, long downloadedLength) {

        progressBar.setProgress((int) percent);
        Log.e("ABCC", percent + "");

        Log.e("ABCC", downloadedLength + "");
    }

    @Override
    public void OnDownloadFinished(long taskId) {
    }

    @Override
    public void OnDownloadRebuildStart(long taskId) {

    }

    @Override
    public void OnDownloadRebuildFinished(long taskId) {

    }

    @Override
    public void OnDownloadCompleted(long taskId) {
        Intent intent = new Intent("DOWNLOAD");
        sendBroadcast(intent);
    }

    @Override
    public void connectionLost(long taskId) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
