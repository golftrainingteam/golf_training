package com.maxsystem.golftraining.student.Models;

import android.graphics.Bitmap;

/**
 * Created by minhdinh on 9/26/16.
 */
public class VideoItem {

    private int  mId;

    private String mDestiption;

    private Bitmap mThumnail;

    public VideoItem(int id, String desctiption, Bitmap thumnail){
        mId = id;
        mDestiption = desctiption;
        mThumnail = thumnail;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmDestiption() {
        return mDestiption;
    }

    public void setmDestiption(String mDestiption) {
        this.mDestiption = mDestiption;
    }

    public Bitmap getmThumnail() {
        return mThumnail;
    }

    public void setmThumnail(Bitmap mThumnail) {
        this.mThumnail = mThumnail;
    }
}
