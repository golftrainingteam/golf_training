package com.maxsystem.golftraining.student;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.maxsystem.golftraining.BaseActivity;
import com.maxsystem.golftraining.PlayVideoActivity;
import com.maxsystem.golftraining.R;
import com.maxsystem.golftraining.camera.CameraActivity;
import com.maxsystem.golftraining.student.Models.VideoItem;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by minhdinh on 9/20/16.
 */
public class ListVideoActivity extends BaseActivity {

    private static final String TAG = ListVideoActivity.class.getSimpleName();

    private ListView mListView;

    private Button mbutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listvideo_activity);
        initView();
        setListener();
    }

    /**
     * init view
     */
    private void initView(){
        mListView = (ListView)findViewById(R.id.lst_video);
        mListView.setAdapter(new ListVideoApdater(ListVideoActivity.this, getListVideo()));
        mbutton = (Button)findViewById(R.id.btn_playvideo);
    }
    /**
     * add listener for view item
     */
    private void setListener(){
        mbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListVideoActivity.this, CameraActivity.class));
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(ListVideoActivity.this, PlayVideoActivity.class));
                Toast.makeText(ListVideoActivity.this, "Feature play video will be add on next version", Toast.LENGTH_LONG).show();
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListVideoActivity.this, "Feature delete video will be add on next version", Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }

    /**
     * get all listvideo on devices
     * @return
     */
    private  List<VideoItem> getListVideo(){
        List<VideoItem> listVideo = new ArrayList<>();
        ContentResolver cr = getContentResolver();
        String[] projection = { MediaStore.Video.Media._ID,MediaStore.Video.VideoColumns.DESCRIPTION,MediaStore.Video.Thumbnails.DATA};
        Cursor cursor = cr.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection,
                null, // Return all rows
                null, null);
        if(cursor == null){
            return listVideo;
        }
        Log.e(TAG, "Count = " + cursor.getCount());
        while (cursor.moveToNext()){
            int id = cursor.getInt(0);
            String description = cursor.getColumnName(1);
            Bitmap decodedByte  = MediaStore.Video.Thumbnails.getThumbnail(cr, id, MediaStore.Video.Thumbnails.MINI_KIND, null);
            VideoItem  video = new VideoItem(id,description,decodedByte);
            listVideo.add(video);

        }
        Log.e(TAG,"Count = "+ listVideo.size());
        cursor.close();
        return listVideo;
    }

    // class adapter for list video
    private class ListVideoApdater extends BaseAdapter{

        private List<VideoItem> data ;

        private  Context context;

        ListVideoApdater(Context context,List<VideoItem> data){
            this.context = context;
            this.data = data;
        }
        @Override
        public int getCount() {
            return data.size();
        }


        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_video,null);
            ((ImageView)view.findViewById(R.id.imv_thumnail)).setImageBitmap(data.get(position).getmThumnail());
            ((TextView)view.findViewById(R.id.tv_name)).setText(data.get(position).getmDestiption());
            ((TextView)view.findViewById(R.id.tv_des)).setText("Video Demo");
            return view;
        }
    }
}
